graph [
  node [
    id 0
    label "0"
    config [
      node_id "e3e7c2094cac629f6fbed82c07cd"
      ipv6 "fc00:e3e7:c209:4cac:629f:6fbe:d82c:07cd"
      name "k0"
      image "kira-small-k"
    ]
  ]
  node [
    id 1
    label "1"
    config [
      node_id "f72842485e3a0a5d2f346baa9455"
      ipv6 "fc00:f728:4248:5e3a:0a5d:2f34:6baa:9455"
      name "k1"
      image "kira-small-k"
    ]
  ]
  node [
    id 2
    label "2"
    config [
      node_id "eb1167a9c3787c65c1e582e2e662"
      ipv6 "fc00:eb11:67a9:c378:7c65:c1e5:82e2:e662"
      name "k2"
      image "kira-small-k"
    ]
  ]
  node [
    id 3
    label "3"
    config [
      node_id "f7c14da5e709d4713d60c8a70639"
      ipv6 "fc00:f7c1:4da5:e709:d471:3d60:c8a7:0639"
      name "k3"
      image "kira-small-k"
    ]
  ]
  node [
    id 4
    label "4"
    config [
      node_id "e4439558867f5ba91faf7a024204"
      ipv6 "fc00:e443:9558:867f:5ba9:1faf:7a02:4204"
      name "k4"
      image "kira-small-k"
    ]
  ]
  node [
    id 5
    label "5"
    config [
      node_id "23a78133287637ebdcd9e87a1613"
      ipv6 "fc00:23a7:8133:2876:37eb:dcd9:e87a:1613"
      name "k5"
      image "kira-small-k"
    ]
  ]
  node [
    id 6
    label "6"
    config [
      node_id "1846c17c627923c6612f48268673"
      ipv6 "fc00:1846:c17c:6279:23c6:612f:4826:8673"
      name "k6"
      image "kira-small-k"
    ]
  ]
  node [
    id 7
    label "7"
    config [
      node_id "fcbd40212ef7cca5a5a19e4d6e3c"
      ipv6 "fc00:fcbd:4021:2ef7:cca5:a5a1:9e4d:6e3c"
      name "k7"
      image "kira-small-k"
    ]
  ]
  node [
    id 8
    label "8"
    config [
      node_id "b486fb97d43588561712e8e5216a"
      ipv6 "fc00:b486:fb97:d435:8856:1712:e8e5:216a"
      name "k8"
      image "kira-small-k"
    ]
  ]
  node [
    id 9
    label "9"
    config [
      node_id "259fe6f4590b9a164106cf6a659e"
      ipv6 "fc00:259f:e6f4:590b:9a16:4106:cf6a:659e"
      name "k9"
      image "kira-small-k"
    ]
  ]
  node [
    id 10
    label "10"
    config [
      node_id "12e0bad640fb19488dec4f65d4d9"
      ipv6 "fc00:12e0:bad6:40fb:1948:8dec:4f65:d4d9"
      name "k10"
      image "kira-small-k"
    ]
  ]
  node [
    id 11
    label "11"
    config [
      node_id "5487af19922ad9b8a714e61a441c"
      ipv6 "fc00:5487:af19:922a:d9b8:a714:e61a:441c"
      name "k11"
      image "kira-small-k"
    ]
  ]
  node [
    id 12
    label "12"
    config [
      node_id "5a9219c78df48f4ff31e78de5857"
      ipv6 "fc00:5a92:19c7:8df4:8f4f:f31e:78de:5857"
      name "k12"
      image "kira-small-k"
    ]
  ]
  node [
    id 13
    label "13"
    config [
      node_id "a3f29c6316b950f244556f25e2a2"
      ipv6 "fc00:a3f2:9c63:16b9:50f2:4455:6f25:e2a2"
      name "k13"
      image "kira-small-k"
    ]
  ]
  node [
    id 14
    label "14"
    config [
      node_id "8d72f77383c13458a748e9bb17bc"
      ipv6 "fc00:8d72:f773:83c1:3458:a748:e9bb:17bc"
      name "k14"
      image "kira-small-k"
    ]
  ]
  node [
    id 15
    label "15"
    config [
      node_id "8577dd84f39e71545a137a1d5006"
      ipv6 "fc00:8577:dd84:f39e:7154:5a13:7a1d:5006"
      name "k15"
      image "kira-small-k"
    ]
  ]
  node [
    id 16
    label "16"
    config [
      node_id "eb20ce164dba0ff18e0242af9fc3"
      ipv6 "fc00:eb20:ce16:4dba:0ff1:8e02:42af:9fc3"
      name "k16"
      image "kira-small-k"
    ]
  ]
  node [
    id 17
    label "17"
    config [
      node_id "17e003983ca8ea7e9d498c778ea6"
      ipv6 "fc00:17e0:0398:3ca8:ea7e:9d49:8c77:8ea6"
      name "k17"
      image "kira-small-k"
    ]
  ]
  node [
    id 18
    label "18"
    config [
      node_id "b5d366194cb1d71037d1b83e90ec"
      ipv6 "fc00:b5d3:6619:4cb1:d710:37d1:b83e:90ec"
      name "k18"
      image "kira-small-k"
    ]
  ]
  node [
    id 19
    label "19"
    config [
      node_id "a011ab0c1681c8f8e3d0d3290a4c"
      ipv6 "fc00:a011:ab0c:1681:c8f8:e3d0:d329:0a4c"
      name "k19"
      image "kira-small-k"
    ]
  ]
  node [
    id 20
    label "20"
    config [
      node_id "d3fb7e5b1e7f9ca5499d004ae545"
      ipv6 "fc00:d3fb:7e5b:1e7f:9ca5:499d:004a:e545"
      name "k20"
      image "kira-small-k"
    ]
  ]
  node [
    id 21
    label "21"
    config [
      node_id "baf33e70f16a55485822de1b372a"
      ipv6 "fc00:baf3:3e70:f16a:5548:5822:de1b:372a"
      name "k21"
      image "kira-small-k"
    ]
  ]
  node [
    id 22
    label "22"
    config [
      node_id "101fded733e8b421eaeb534097ca"
      ipv6 "fc00:101f:ded7:33e8:b421:eaeb:5340:97ca"
      name "k22"
      image "kira-small-k"
    ]
  ]
  node [
    id 23
    label "23"
    config [
      node_id "38c19148624feac1c14f30e9c5cc"
      ipv6 "fc00:38c1:9148:624f:eac1:c14f:30e9:c5cc"
      name "k23"
      image "kira-small-k"
    ]
  ]
  node [
    id 24
    label "24"
    config [
      node_id "247af7b0b7d2cda8056c3d15eef7"
      ipv6 "fc00:247a:f7b0:b7d2:cda8:056c:3d15:eef7"
      name "k24"
      image "kira-small-k"
    ]
  ]
  node [
    id 25
    label "25"
    config [
      node_id "175972ae22448b0163c1cd9d2b7d"
      ipv6 "fc00:1759:72ae:2244:8b01:63c1:cd9d:2b7d"
      name "k25"
      image "kira-small-k"
    ]
  ]
  node [
    id 26
    label "26"
    config [
      node_id "e00551ef1922fe43c49e149818d1"
      ipv6 "fc00:e005:51ef:1922:fe43:c49e:1498:18d1"
      name "k26"
      image "kira-small-k"
    ]
  ]
  node [
    id 27
    label "27"
    config [
      node_id "7d41eece328bff7b118e820865d6"
      ipv6 "fc00:7d41:eece:328b:ff7b:118e:8208:65d6"
      name "k27"
      image "kira-small-k"
    ]
  ]
  node [
    id 28
    label "28"
    config [
      node_id "4a848d1fd9b74d2b9deb1beb3711"
      ipv6 "fc00:4a84:8d1f:d9b7:4d2b:9deb:1beb:3711"
      name "k28"
      image "kira-small-k"
    ]
  ]
  node [
    id 29
    label "29"
    config [
      node_id "552f8c25166a1ff39849b4e1357d"
      ipv6 "fc00:552f:8c25:166a:1ff3:9849:b4e1:357d"
      name "k29"
      image "kira-small-k"
    ]
  ]
  node [
    id 30
    label "30"
    config [
      node_id "34058a5006c1ec188efbd080e66e"
      ipv6 "fc00:3405:8a50:06c1:ec18:8efb:d080:e66e"
      name "k30"
      image "kira-small-k"
    ]
  ]
  node [
    id 31
    label "31"
    config [
      node_id "8c179a6a5f92cca74147f6be1f72"
      ipv6 "fc00:8c17:9a6a:5f92:cca7:4147:f6be:1f72"
      name "k31"
      image "kira-small-k"
    ]
  ]
  node [
    id 32
    label "32"
    config [
      node_id "177571eacd0549a3e80e966e1277"
      ipv6 "fc00:1775:71ea:cd05:49a3:e80e:966e:1277"
      name "k32"
      image "kira-small-k"
    ]
  ]
  node [
    id 33
    label "33"
    config [
      node_id "51296288e1a5cc45782198a6416d"
      ipv6 "fc00:5129:6288:e1a5:cc45:7821:98a6:416d"
      name "k33"
      image "kira-small-k"
    ]
  ]
  node [
    id 34
    label "34"
    config [
      node_id "2f124a5308cc3dfabc08935ddd72"
      ipv6 "fc00:2f12:4a53:08cc:3dfa:bc08:935d:dd72"
      name "k34"
      image "kira-small-k"
    ]
  ]
  node [
    id 35
    label "35"
    config [
      node_id "08702fcd81b5d24bace4307bf326"
      ipv6 "fc00:0870:2fcd:81b5:d24b:ace4:307b:f326"
      name "k35"
      image "kira-small-k"
    ]
  ]
  node [
    id 36
    label "36"
    config [
      node_id "4293a81ad477fb3675b89cdeb3e6"
      ipv6 "fc00:4293:a81a:d477:fb36:75b8:9cde:b3e6"
      name "k36"
      image "kira-small-k"
    ]
  ]
  node [
    id 37
    label "37"
    config [
      node_id "adc016febaa011af923d79fdef7c"
      ipv6 "fc00:adc0:16fe:baa0:11af:923d:79fd:ef7c"
      name "k37"
      image "kira-small-k"
    ]
  ]
  node [
    id 38
    label "38"
    config [
      node_id "2648e07405eb215663abc1f254b8"
      ipv6 "fc00:2648:e074:05eb:2156:63ab:c1f2:54b8"
      name "k38"
      image "kira-small-k"
    ]
  ]
  node [
    id 39
    label "39"
    config [
      node_id "148bd7ab792809e469e6ec62b2c8"
      ipv6 "fc00:148b:d7ab:7928:09e4:69e6:ec62:b2c8"
      name "k39"
      image "kira-small-k"
    ]
  ]
  node [
    id 40
    label "40"
    config [
      node_id "d450ec4f217bb306d1a8e5eeac76"
      ipv6 "fc00:d450:ec4f:217b:b306:d1a8:e5ee:ac76"
      name "k40"
      image "kira-small-k"
    ]
  ]
  node [
    id 41
    label "41"
    config [
      node_id "d67e642bfa42aef9c00b8a64c1b9"
      ipv6 "fc00:d67e:642b:fa42:aef9:c00b:8a64:c1b9"
      name "k41"
      image "kira-small-k"
    ]
  ]
  node [
    id 42
    label "42"
    config [
      node_id "8594468ff53d864a7a50b48d73f1"
      ipv6 "fc00:8594:468f:f53d:864a:7a50:b48d:73f1"
      name "k42"
      image "kira-small-k"
    ]
  ]
  node [
    id 43
    label "43"
    config [
      node_id "3717d977e9933c49d76fcfc6e625"
      ipv6 "fc00:3717:d977:e993:3c49:d76f:cfc6:e625"
      name "k43"
      image "kira-small-k"
    ]
  ]
  node [
    id 44
    label "44"
    config [
      node_id "d34496fd35d0adf20806e5214606"
      ipv6 "fc00:d344:96fd:35d0:adf2:0806:e521:4606"
      name "k44"
      image "kira-small-k"
    ]
  ]
  node [
    id 45
    label "45"
    config [
      node_id "46749466e4726b5f5241f323ca74"
      ipv6 "fc00:4674:9466:e472:6b5f:5241:f323:ca74"
      name "k45"
      image "kira-small-k"
    ]
  ]
  node [
    id 46
    label "46"
    config [
      node_id "a425a905d7507e1ea9c573581a81"
      ipv6 "fc00:a425:a905:d750:7e1e:a9c5:7358:1a81"
      name "k46"
      image "kira-small-k"
    ]
  ]
  node [
    id 47
    label "47"
    config [
      node_id "fb82eabca8d0b341facdff0ac0f1"
      ipv6 "fc00:fb82:eabc:a8d0:b341:facd:ff0a:c0f1"
      name "k47"
      image "kira-small-k"
    ]
  ]
  node [
    id 48
    label "48"
    config [
      node_id "5306151665705b7c709acb175a5a"
      ipv6 "fc00:5306:1516:6570:5b7c:709a:cb17:5a5a"
      name "k48"
      image "kira-small-k"
    ]
  ]
  node [
    id 49
    label "49"
    config [
      node_id "964a7c879b741d878f9f9cdf5a86"
      ipv6 "fc00:964a:7c87:9b74:1d87:8f9f:9cdf:5a86"
      name "k49"
      image "kira-small-k"
    ]
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 37
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 42
    fail 1
  ]
  edge [
    source 34
    target 43
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 47
    target 49
  ]
]
