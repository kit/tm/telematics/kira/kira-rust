graph [
  node [
    id 0
    label "0"
    config [
      node_id "e3e7c2094cac629f6fbed82c07cd"
      ipv6 "fc00:e3e7:c209:4cac:629f:6fbe:d82c:07cd"
      name "k0"
      image "kira"
    ]
  ]
  node [
    id 1
    label "1"
    config [
      node_id "f72842485e3a0a5d2f346baa9455"
      ipv6 "fc00:f728:4248:5e3a:0a5d:2f34:6baa:9455"
      name "k1"
      image "kira"
    ]
  ]
  node [
    id 2
    label "2"
    config [
      node_id "eb1167a9c3787c65c1e582e2e662"
      ipv6 "fc00:eb11:67a9:c378:7c65:c1e5:82e2:e662"
      name "k2"
      image "kira"
    ]
  ]
  node [
    id 3
    label "3"
    config [
      node_id "f7c14da5e709d4713d60c8a70639"
      ipv6 "fc00:f7c1:4da5:e709:d471:3d60:c8a7:0639"
      name "k3"
      image "kira"
    ]
  ]
  node [
    id 4
    label "4"
    config [
      node_id "e4439558867f5ba91faf7a024204"
      ipv6 "fc00:e443:9558:867f:5ba9:1faf:7a02:4204"
      name "k4"
      image "kira"
    ]
  ]
  node [
    id 5
    label "5"
    config [
      node_id "23a78133287637ebdcd9e87a1613"
      ipv6 "fc00:23a7:8133:2876:37eb:dcd9:e87a:1613"
      name "k5"
      image "kira"
    ]
  ]
  node [
    id 6
    label "6"
    config [
      node_id "1846c17c627923c6612f48268673"
      ipv6 "fc00:1846:c17c:6279:23c6:612f:4826:8673"
      name "k6"
      image "kira"
    ]
  ]
  node [
    id 7
    label "7"
    config [
      node_id "fcbd40212ef7cca5a5a19e4d6e3c"
      ipv6 "fc00:fcbd:4021:2ef7:cca5:a5a1:9e4d:6e3c"
      name "k7"
      image "kira"
    ]
  ]
  node [
    id 8
    label "8"
    config [
      node_id "b486fb97d43588561712e8e5216a"
      ipv6 "fc00:b486:fb97:d435:8856:1712:e8e5:216a"
      name "k8"
      image "kira"
    ]
  ]
  node [
    id 9
    label "9"
    config [
      node_id "259fe6f4590b9a164106cf6a659e"
      ipv6 "fc00:259f:e6f4:590b:9a16:4106:cf6a:659e"
      name "k9"
      image "kira"
    ]
  ]
  node [
    id 10
    label "10"
    config [
      node_id "12e0bad640fb19488dec4f65d4d9"
      ipv6 "fc00:12e0:bad6:40fb:1948:8dec:4f65:d4d9"
      name "k10"
      image "kira"
    ]
  ]
  node [
    id 11
    label "11"
    config [
      node_id "5487af19922ad9b8a714e61a441c"
      ipv6 "fc00:5487:af19:922a:d9b8:a714:e61a:441c"
      name "k11"
      image "kira"
    ]
  ]
  node [
    id 12
    label "12"
    config [
      node_id "5a9219c78df48f4ff31e78de5857"
      ipv6 "fc00:5a92:19c7:8df4:8f4f:f31e:78de:5857"
      name "k12"
      image "kira"
    ]
  ]
  node [
    id 13
    label "13"
    config [
      node_id "a3f29c6316b950f244556f25e2a2"
      ipv6 "fc00:a3f2:9c63:16b9:50f2:4455:6f25:e2a2"
      name "k13"
      image "kira"
    ]
  ]
  node [
    id 14
    label "14"
    config [
      node_id "8d72f77383c13458a748e9bb17bc"
      ipv6 "fc00:8d72:f773:83c1:3458:a748:e9bb:17bc"
      name "k14"
      image "kira"
    ]
  ]
  node [
    id 15
    label "15"
    config [
      node_id "8577dd84f39e71545a137a1d5006"
      ipv6 "fc00:8577:dd84:f39e:7154:5a13:7a1d:5006"
      name "k15"
      image "kira"
    ]
  ]
  node [
    id 16
    label "16"
    config [
      node_id "eb20ce164dba0ff18e0242af9fc3"
      ipv6 "fc00:eb20:ce16:4dba:0ff1:8e02:42af:9fc3"
      name "k16"
      image "kira"
    ]
  ]
  node [
    id 17
    label "17"
    config [
      node_id "17e003983ca8ea7e9d498c778ea6"
      ipv6 "fc00:17e0:0398:3ca8:ea7e:9d49:8c77:8ea6"
      name "k17"
      image "kira"
    ]
  ]
  node [
    id 18
    label "18"
    config [
      node_id "b5d366194cb1d71037d1b83e90ec"
      ipv6 "fc00:b5d3:6619:4cb1:d710:37d1:b83e:90ec"
      name "k18"
      image "kira"
    ]
  ]
  node [
    id 19
    label "19"
    config [
      node_id "a011ab0c1681c8f8e3d0d3290a4c"
      ipv6 "fc00:a011:ab0c:1681:c8f8:e3d0:d329:0a4c"
      name "k19"
      image "kira"
    ]
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 17
    target 19
  ]
]
