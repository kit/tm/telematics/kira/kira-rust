graph [
  node [
    id 0
    label "0"
    config [
      node_id "e3e7c2094cac629f6fbed82c07cd"
      ipv6 "fc00:e3e7:c209:4cac:629f:6fbe:d82c:07cd"
      name "k0"
      image "kira"
    ]
  ]
  node [
    id 1
    label "1"
    config [
      node_id "f72842485e3a0a5d2f346baa9455"
      ipv6 "fc00:f728:4248:5e3a:0a5d:2f34:6baa:9455"
      name "k1"
      image "kira"
    ]
  ]
  edge [
    source 0
    target 1
  ]
]
