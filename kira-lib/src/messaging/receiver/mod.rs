use std::collections::HashSet;
use std::error::Error;
use std::fmt::{Debug, Display};
use std::time::Duration;

use crate::domain::NetworkInterface;
use crate::messaging::messages::ProtocolMessage;

#[cfg(feature = "udp-tokio")]
pub mod udp_tokio;

/// Error type for [ProtocolMessageReceiver::recv] and [AsyncProtocolMessageReceiver::recv].
#[derive(Debug)]
pub enum RecvError {
    /// Receiving timed out.
    Timeout,
    /// Returned if no interface for a message was found.
    ///
    /// This may signal an inconsistency in the interface configuration.
    NoInterfaceFound,
    /// Signals that one or many interfaces stopped working.
    ///
    /// This doesn't signal that the receiver stops operating.
    InterfacesDown(HashSet<NetworkInterface>),
    /// Signals that no more messages will be received from this receiver.
    ///
    /// Also includes the remaining interfaces this receiver handled.
    Closed(HashSet<NetworkInterface>),
    /// The I/O-Layer returned some error.
    IoError(Box<dyn Error + Send>),
    /// Other Error for custom error types of the implementations.
    Other(Box<dyn Error + Send>),
}

impl Display for RecvError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Timeout => write!(f, "Timeout receiving a Message"),
            Self::NoInterfaceFound => write!(
                f,
                "No interface for message was found; Network configuration may be inconsistent"
            ),
            Self::IoError(e) => write!(f, "Received IO Error: {}", e),
            Self::Other(e) => write!(f, "{}", e),
            Self::InterfacesDown(interfaces) => {
                write!(f, "Interfaces {:?} stopped working", interfaces)
            }
            Self::Closed(interfaces) => {
                write!(f, "Receiver for interfaces {:?} closed", interfaces)
            }
        }
    }
}

impl Error for RecvError {}

/// Error type for [ProtocolMessageReceiver::try_recv] and
/// [ProtocolMessageReceiver::try_recv](crate::messaging::receiver::ProtocolMessageReceiver::try_recv).
#[derive(Debug)]
pub enum TryRecvError {
    /// The I/O-Layer returned some error.
    IoError(Box<dyn Error + Send>),
    /// Returned if no interface for a message was found.
    ///
    /// This may signal an inconsistency in the interface configuration.
    NoInterfaceFound,
    /// Signals that one or many interfaces stopped working.
    ///
    /// This doesn't signal that the receiver stops operating.
    InterfacesDown(HashSet<NetworkInterface>),
    /// Signals that no more messages will be received from this receiver.
    ///
    /// Also includes the remaining interfaces this receiver handled.
    Closed(HashSet<NetworkInterface>),
    /// Other Error for custom error types of the implementations.
    Other(Box<dyn Error + Send>),
}

impl Display for TryRecvError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IoError(e) => write!(f, "Received IO Error: {}", e),
            Self::NoInterfaceFound => write!(
                f,
                "No interface for message was found; Network configuration may be inconsistent"
            ),
            Self::Other(e) => write!(f, "{}", e),
            Self::InterfacesDown(interfaces) => {
                write!(f, "Interfaces {:?} stopped working", interfaces)
            }
            Self::Closed(interfaces) => {
                write!(f, "Receiver for interfaces {:?} closed", interfaces)
            }
        }
    }
}

impl Error for TryRecvError {}

/// Receives [ProtocolMessage]s of other Nodes.
///
/// A single [ProtocolMessageReceiver] can be responsible for one or many [NetworkInterface]s.
///
/// Converts a [ProtocolMessage] formatted by its corresponding
/// [ProtocolMessageSender](crate::messaging::sender::ProtocolMessageSender) back
/// to a [ProtocolMessage] and returns it.
///
/// Every method is allowed to return [None] at any point in time.
/// In cases where the [ProtocolMessageReceiver] is no longer able to receive messages
/// an error has to be returned.
pub trait ProtocolMessageReceiver {
    /// Receives a [ProtocolMessage].
    ///
    /// Returns an [Error] if receiving failed or the optional timeout was reached.
    ///
    /// If no timeout was given the operation waits until a new [ProtocolMessage] arrived.
    ///
    /// Returns [None] if no messages will be received from this [ProtocolMessageReceiver] anymore.
    fn recv_timeout(
        &mut self,
        timeout: Option<Duration>,
    ) -> Result<Option<(ProtocolMessage, NetworkInterface)>, RecvError>;
    /// Receives a [ProtocolMessage].
    ///
    /// Short for calling [recv_timeout](ProtocolMessageReceiver::recv_timeout) with [None].
    fn recv(&mut self) -> Result<Option<(ProtocolMessage, NetworkInterface)>, RecvError> {
        self.recv_timeout(None)
    }
    /// Tries to receive a [ProtocolMessage] and returns an [Error] if no message is present at the time.
    fn try_recv(&mut self) -> Result<Option<(ProtocolMessage, NetworkInterface)>, TryRecvError>;
}

/// Receives [ProtocolMessage]s of other Nodes.
///
/// Converts a [ProtocolMessage] formatted by its corresponding
/// [ProtocolMessageSender](crate::messaging::sender::ProtocolMessageSender) back
/// to a [ProtocolMessage] and returns it.
#[async_trait::async_trait]
pub trait AsyncProtocolMessageReceiver {
    /// Receives a [ProtocolMessage].
    ///
    /// Returns an [Error] if receiving failed or the optional timeout was reached.
    ///
    /// If no timeout was given the operation waits until a new [ProtocolMessage] arrived.
    ///
    /// Returns [None] if no messages will be received from this [ProtocolMessageReceiver] anymore.
    async fn recv_timeout(
        &mut self,
        timeout: Option<Duration>,
    ) -> Result<Option<(ProtocolMessage, NetworkInterface)>, RecvError>;
    /// Receives a [ProtocolMessage].
    ///
    /// Short for calling [recv_timeout](ProtocolMessageReceiver::recv_timeout) with
    /// [None].
    async fn recv(&mut self) -> Result<Option<(ProtocolMessage, NetworkInterface)>, RecvError>;
    /// Tries to receive a [ProtocolMessage] and returns an [Error] if no message is present at the time.
    async fn try_recv(
        &mut self,
    ) -> Result<Option<(ProtocolMessage, NetworkInterface)>, TryRecvError>;
}
